import React from 'react';

export default React.createClass({
    componentDidMount() {
        require('../css/Drinks/DrinkItem.css');
    },

    render() {
        return (
            <div className="DrinkItem">
                <span class="drink-name">{this.props.drinkName}</span> 
                <span class="drink-cost">Cost {this.props.drinkCost}</span>
            </div>
        );
    }
});
