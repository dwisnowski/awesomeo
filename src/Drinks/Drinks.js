import React from 'react';
import DrinkItem from './DrinkItem';

//
// function displayAllDrinks() {
//     var drinkNames = ['Drink 1', 'Drink 2', 'Drink 3', 'Drink 4', 'Drink 5'];
//
//     var drinkItems = drinkNames.map((drinkName) => {
//         return (
//             <DrinkItem drinkName={drinkName}/>
//         );
//     });
//
//     return drinkItems;
// }

function getDrinksFromDatabase() {
    return [
        {
            name: 'Drink 1',
            cost: 10,
            color: 'red'
        }, {
            name: 'Drink 2',
            cost: 5,
            color: 'blue'
        }
    ];
}



function displayAllDrinks() {
    var drinks = getDrinksFromDatabase();

    var drinkItems = drinks.map((drink) => {
        return (
            <DrinkItem drinkName={drink.name} drinkCost={drink.cost}/>
        );
    });

    return drinkItems;
}


export default React.createClass({
    componentDidMount() {
        require('../css/Drinks/Drinks.css');
    },
    
    render() {
        return (
            <div>
                <h2> Drinks </h2>
                {displayAllDrinks()}
            </div>
        );
    }
});
